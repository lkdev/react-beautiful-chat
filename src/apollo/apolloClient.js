import {
  ApolloClient,
  ApolloLink,
  InMemoryCache,
  HttpLink,
  split,
} from 'apollo-boost';
import { WebSocketLink } from 'apollo-link-ws';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { createUploadLink } from 'apollo-upload-client';
import { getMainDefinition } from 'apollo-utilities';

import config from '../config';

export const cache = new InMemoryCache({
  // @ts-ignore
  dataIdFromObject: o => o._id || o.id,
});

const authLink = setContext(async (req, { headers }) => {
  const token =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2FjMjRhMWJmN2YzMjRhNTA4ZjhhMjYiLCJvcmdhbml6YXRpb25JZCI6IjVjYWMyNDY3ODAxZDAwNGEyZDNmMTk4OSIsInJvbGUiOiJhZG1pbiIsImlhdCI6MTU1OTM0MjE0MCwiZXhwIjoxNTYxOTM0MTQwfQ.q5Hu5NylZG3csQkcCCLYuTwo1i-UMrpkAfasmtPWt2Q';
  return {
    ...headers,
    headers: { authorization: token ? `Bearer ${token}` : '' },
  };
});

// @ts-ignore
const errorLink = onError(({ networkError, graphQLErrors }) => {
  if (networkError) {
    console.error('NETWORK_ERROR', networkError); // eslint-disable-line
    // return forward(operation);
  }
  if (graphQLErrors) {
    graphQLErrors.map(async error => {
      const { extensions, message } = error;
      // eslint-disable-next-line no-console
      console.log('extensions:', extensions);
      // eslint-disable-line
      console.log(`APOLLO_ERROR: message -> ${message}, more ->`, error); // eslint-disable-line
      return null;
    });
  }
  return null;
});

const token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2FjMjRhMWJmN2YzMjRhNTA4ZjhhMjYiLCJvcmdhbml6YXRpb25JZCI6IjVjYWMyNDY3ODAxZDAwNGEyZDNmMTk4OSIsInJvbGUiOiJhZG1pbiIsImlhdCI6MTU1OTM0MjE0MCwiZXhwIjoxNTYxOTM0MTQwfQ.q5Hu5NylZG3csQkcCCLYuTwo1i-UMrpkAfasmtPWt2Q';
const wsLink = new WebSocketLink({
  uri: config.GRAPHQL_SUBSCRIPTION_ENDPOINT,
  options: {
    reconnect: true,
    connectionParams: {
      authToken: token,
    },
  },
});

const uploadLink = createUploadLink({ uri: config.GRAPHQL_ENDPOINT });

const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  uploadLink
);

const apolloClient = new ApolloClient({
  link: authLink.concat(ApolloLink.from([errorLink, link])),
  cache,
  resolvers: {
    Mutation: {},
  },
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'cache-and-network',
      errorPolicy: 'ignore',
    },
    query: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
  },
});

cache.writeData({
  data: {
    messages: [],
  },
});

export default apolloClient;
