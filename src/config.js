const config = {
  GRAPHQL_ENDPOINT: 'http://localhost:4000/graphql',
  GRAPHQL_SUBSCRIPTION_ENDPOINT: 'ws://localhost:4000/subscriptions',
};

console.log('MODE', process.env.NODE_ENV);

if (process.env.NODE_ENV === 'production') {
  config.GRAPHQL_ENDPOINT = 'https://botility.com/graphql';
  config.GRAPHQL_SUBSCRIPTION_ENDPOINT = 'wss://botility.com/subscriptions';
}

export default config;
