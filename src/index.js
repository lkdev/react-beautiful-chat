import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import './styles';
// import 'bootstrap/dist/css/bootstrap.css';
import App from './containers/App/index';

export default {
  widgets: {
    chatWidget: () => {
      const metaTag = document.getElementById('viewport');
      if (!metaTag) {
        const Head = document.getElementsByTagName('head')[0];
        const Meta = document.createElement('meta');
        Meta.setAttribute('name', 'viewport');
        Meta.setAttribute('content', 'width=device-width, initial-scale=1.0');
        Head.appendChild(Meta);
      }
      return {
        render: () => {
          ReactDOM.render(<App />, document.querySelector('#root'));
        },
        unmount() {
          ReactDOM.unmountComponentAtNode(document.querySelector('#root'));
        },
      };
    },
  },
};
