import PropTypes from 'prop-types';
import React, { Component } from 'react';

import Router from './Router';
import ChatBox from './ChatBox';

class ChatWindow extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const classList = [
      'sc-chat-window',
      this.props.isOpen ? 'opened' : 'closed',
    ];
    return (
      <div className={classList.join(' ')}>
        <Router {...this.props} />
      </div>
    );
  }
}

ChatWindow.propTypes = {
  showEmoji: PropTypes.bool,
  showFile: PropTypes.bool,
  onKeyPress: PropTypes.func,
};

export default ChatWindow;
