import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { render } from 'react-dom';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import SendIcon from './icons/SendIcon';
// import EmojiIcon from './icons/EmojiIcon';
// import EmojiPicker from './emoji-picker/EmojiPicker';
import FileIcons from './icons/FileIcon';

const WEBCHAT_MESSAGE_SAVE_MUTATION = gql`
  mutation webchatMessageSave($messageBody: String!, $conversationId: String!) {
    webchatMessageSave(
      messageBody: $messageBody
      conversationId: $conversationId
    ) {
      _id
      senderId
      recepientId
      conversationId
      messageBody
      messageType
      attachmentName
      attachmentUrl
      attachmentCaption
      attachmentSize
      platformName
      createdAt
      organizationId
    }
  }
`;

class UserInput extends Component {
  constructor() {
    super();
    this.state = {
      inputActive: false,
      file: null,
    };
  }

  handleKey = event => {
    if (event.keyCode === 13 && !event.shiftKey) {
      this._submitText(event);
    }
  };

  // handleKeyPress = _.debounce(
  //   () => {
  //     this.props.onKeyPress(this.userInput.textContent);
  //   },
  //   300,
  //   { trailing: true }
  // );

  _submitText(event) {
    event.preventDefault();
    const text = this.userInput.textContent;
    const { file } = this.state;
    if (file) {
      if (text && text.length > 0) {
        this.props.onSubmit({
          author: 'me',
          type: 'file',
          data: { text, file },
        });
        this.setState({ file: null });
        this.userInput.innerHTML = '';
      } else {
        this.props.onSubmit({
          author: 'me',
          type: 'file',
          data: { file },
        });
        this.setState({ file: null });
      }
    } else if (text && text.length > 0) {
      const me = JSON.parse(sessionStorage.getItem('webchat_me'));
      this.props
        .webchatMessageSave({
          variables: {
            messageBody: text,
            conversationId: me.conversationId,
          },
          update(
            cache,
            {
              data: { webchatMessageSave },
            }
          ) {
            // console.log(webchatMessageSave);
          },
        })
        .catch(err => console.log(err));
      this.userInput.innerHTML = '';
    }
  }

  // _handleEmojiPicked(emoji) {
  //   this.props.onSubmit({
  //     author: 'me',
  //     type: 'emoji',
  //     data: { emoji },
  //   });
  // }

  _handleFileSubmit(file) {
    this.setState({ file });
  }

  render() {
    return (
      <div>
        {this.state.file && (
          <div className="file-container">
            <span className="icon-file-message">
              <img
                src="https://drbot.sfo2.digitaloceanspaces.com/media/file.svg"
                alt="genericFileIcon"
                height={15}
              />
            </span>
            {this.state.file && this.state.file.name}
            <span
              className="delete-file-message"
              onClick={() => this.setState({ file: null })}
            >
              <img
                src="https://drbot.sfo2.digitaloceanspaces.com/media/close-icon.png"
                alt="close icon"
                height={10}
                title="Remove the file"
              />
            </span>
          </div>
        )}
        <form
          className={`sc-user-input ${this.state.inputActive ? 'active' : ''}`}
        >
          <div
            role="button"
            tabIndex="0"
            onFocus={() => {
              this.setState({ inputActive: true });
            }}
            onBlur={() => {
              this.setState({ inputActive: false });
            }}
            ref={e => {
              this.userInput = e;
            }}
            onKeyDown={this.handleKey}
            // onKeyPress={this.handleKeyPress}
            contentEditable="true"
            placeholder="Write a reply..."
            className="sc-user-input--text"
          />
          <div className="sc-user-input--buttons">
            <div className="sc-user-input--button" />
            <div className="sc-user-input--button">
              {/* {this.props.showEmoji && (
                <EmojiIcon onEmojiPicked={this._handleEmojiPicked.bind(this)} />
              )} */}
            </div>
            {this.props.showFile && (
              <div className="sc-user-input--button">
                <FileIcons onChange={file => this._handleFileSubmit(file)} />
              </div>
            )}
            <div className="sc-user-input--button">
              <SendIcon onClick={this._submitText.bind(this)} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

UserInput.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  showEmoji: PropTypes.bool,
  showFile: PropTypes.bool,
  onKeyPress: PropTypes.func,
};

UserInput.defaultProps = {
  showEmoji: true,
  showFile: true,
};

export default graphql(WEBCHAT_MESSAGE_SAVE_MUTATION, {
  name: 'webchatMessageSave',
})(UserInput);
