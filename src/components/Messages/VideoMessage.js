/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react';

const VideoMessage = props => {
  const { attachmentUrl, attachmentName, attachmentCaption } = props.message;
  return (
    <div className="sc-message--file">
      <div>
        {/* <a href={attachmentUrl || '#'} target="_blank"> */}
        <video className="responsive" autoPlay controls>
          <source src={attachmentUrl} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
        {/* </a> */}
      </div>
      <div className="sc-message--file-text">{attachmentCaption || ''}</div>
    </div>
  );
};

export default VideoMessage;
