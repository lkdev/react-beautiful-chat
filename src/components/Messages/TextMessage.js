import React, { Component } from 'react';

function checkRtl(text) {
  const RTL = [
    'ا',
    'ب',
    'پ',
    'ت',
    'س',
    'ج',
    'چ',
    'ح',
    'خ',
    'د',
    'ذ',
    'ر',
    'ز',
    'ژ',
    'س',
    'ش',
    'ص',
    'ض',
    'ط',
    'ظ',
    'ع',
    'غ',
    'ف',
    'ق',
    'ک',
    'گ',
    'ل',
    'م',
    'ن',
    'و',
    'ه',
    'ی',
  ];
  return text.split('').some(o => RTL.includes(o));
}

const TextMessage = props => {
  // const meta = props.message.data.meta || null;
  const text = props.message.messageBody || '';
  const parsedHtml = text.replace(
    /\*(.*?)\*/gi,
    '<strong class="strong__letters">$1</strong>'
  );

  // for ( var index = 0; index < divs.length; index++ ) {
  //     if( checkRtl( divs[index].textContent[0] ) ) {
  //         divs[index].className = 'rtl';
  //     } else {
  //         divs[index].className = 'ltr';
  //     };
  // };
  const isRtl = checkRtl(text);
  console.log('isRtl:', isRtl);

  return (
    <div className="sc-message--text">
      <p
        style={{ wordBreak: 'break-word', textAlign: isRtl ? 'right' : 'left' }}
        dangerouslySetInnerHTML={{ __html: parsedHtml }} // eslint-disable-line
        dir={isRtl ? 'rtl' : 'ltr'}
      />
      {/* {meta && <p className="sc-message--meta">{meta}</p>} */}
    </div>
  );
};

export default TextMessage;
