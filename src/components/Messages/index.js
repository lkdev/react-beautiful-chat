import React, { Component } from 'react';
import TextMessage from './TextMessage';
import EmojiMessage from './EmojiMessage';
import FileMessage from './FileMessage';
import ImageMessage from './ImageMessage';
import VideoMessage from './VideoMessage';

class Message extends Component {
  _renderMessageOfType(type) {
    switch (type) {
      case 'text':
        return (
          <TextMessage
            message={this.props.message}
            onDelete={this.props.onDelete}
          />
        );
      case 'image':
        return (
          <ImageMessage
            message={this.props.message}
            onDelete={this.props.onDelete}
          />
        );
      case 'video':
        return (
          <VideoMessage
            message={this.props.message}
            onDelete={this.props.onDelete}
          />
        );
      // case 'emoji':
      //   return <EmojiMessage {...this.props.message} />;
      default:
        return (
          <FileMessage
            onDelete={this.props.onDelete}
            message={this.props.message}
          />
        );
    }
  }

  render() {
    const { message } = this.props;
    const contentClassList = [
      'sc-message--content',
      message.senderId !== message.organizationId ? 'sent' : 'received',
    ];
    return (
      <div className="sc-message">
        <div className={contentClassList.join(' ')}>
          <div
            className="sc-message--avatar"
            style={{
              backgroundImage:
                'url(https://drbot.sfo2.digitaloceanspaces.com/media/chat-icon.svg)',
            }}
          />
          {this._renderMessageOfType(this.props.message.messageType)}
        </div>
      </div>
    );
  }
}

export default Message;
