import React from 'react';

const ImageMessage = props => {
  const { attachmentUrl, attachmentName, attachmentCaption } = props.message;
  return (
    <div className="sc-message--file">
      <div className="">
        <a href={attachmentUrl || '#'} target="_blank">
          <img
            className="responsive"
            src={attachmentUrl}
            alt="attachment_image"
            height={60}
          />
        </a>
      </div>
      <div className="sc-message--file-text">{attachmentCaption || ''}</div>
    </div>
  );
};

export default ImageMessage;
