import React from 'react';

const FileMessage = props => {
  const { attachmentUrl, attachmentName, attachmentCaption } = props.message;
  return (
    <div className="sc-message--file">
      <div className="sc-message--file-icon">
        <a href={attachmentUrl || '#'} target="_blank">
          <img
            src="https://drbot.sfo2.digitaloceanspaces.com/media/file.svg"
            alt="generic file icon"
            height={60}
          />
        </a>
      </div>
      <div className='sc-message--file-name'>
        <a href={attachmentUrl} target='_blank'>Open File</a>
      </div>
      {/* <div className="sc-message--file-name">
        <a href={attachmentUrl} target="_blank">
          {attachmentName}
        </a>
      </div>
      <div className="sc-message--file-text">{attachmentCaption || ''}</div> */}
    </div>
  );
};

export default FileMessage;
